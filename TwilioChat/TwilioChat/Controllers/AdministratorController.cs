﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Mvc;
using Twilio;
using Twilio.Rest.Chat.V2;
using Twilio.Rest.Chat.V2.Service;
using Twilio.Rest.Chat.V2.Service.Channel;
using Twilio.Rest.Chat.V2.Service.User;
using TwilioChat.Models;

namespace TwilioChat.Controllers
{
    public class AdministratorController : Controller
    {
        public ViewResult view;
        public ActionResult Index()
        {
            view = View();
            return View();

        }

        [HttpPost]
        public ActionResult Login(FormCollection collection)
        {
            string name = Convert.ToString(collection["username"]);
            TwilioClient.Init(GlobalVariables.accountSid, GlobalVariables.authToken);
            var users = Twilio.Rest.Chat.V2.Service.UserResource.Read(
                pathServiceSid: GlobalVariables.pathServiceSid
            );

            List<string> listUsers = new List<string>();
            TwilioChat.Models.User user = new TwilioChat.Models.User();
            foreach (var record in users)
            {
                listUsers.Add(record.Identity);
            }
            if (listUsers.Count.Equals(0))
                return Redirect(HttpContext.Request.UrlReferrer.AbsoluteUri);
            else
            {
                if (listUsers.Contains(name))
                    return RedirectToAction("Channels");
            }
            return Redirect(HttpContext.Request.UrlReferrer.AbsoluteUri);
        }

        [HttpGet]
        public ActionResult Channels()
        {
            TwilioClient.Init(GlobalVariables.accountSid, GlobalVariables.authToken);
            var channels = ChannelResource.Read(pathServiceSid: GlobalVariables.pathServiceSid);
            Channel channelList = new Channel();
            List<Channel> list = new List<Channel>();
            foreach (var item in channels)
            {
                channelList = new Channel
                {
                    sid = String.IsNullOrEmpty(item.Sid) ? "" : item.Sid.ToString(),
                    account_sid = String.IsNullOrEmpty(item.AccountSid) ? "" : item.AccountSid.ToString(),
                    service_sid = String.IsNullOrEmpty(item.ServiceSid) ? "" : item.ServiceSid.ToString(),
                    friendly_name = String.IsNullOrEmpty(item.FriendlyName) ? "" : item.FriendlyName.ToString(),
                    unique_name = String.IsNullOrEmpty(item.UniqueName) ? "" : item.UniqueName.ToString()
                };
                list.Add(channelList);
            }
            string json = JsonConvert.SerializeObject(list);
            var listado = JsonConvert.DeserializeObject<List<Channel>>(json.ToString());
            return View(listado);
        }
        //
        [HttpGet]
        public ActionResult NewChannel()
        {
            return View();
        }
        //
        [HttpPost]
        public ActionResult NewChannel(Channel channel)
        {
            TwilioClient.Init(GlobalVariables.accountSid, GlobalVariables.authToken);
            var channels = ChannelResource.Create(pathServiceSid: GlobalVariables.pathServiceSid,
                friendlyName: channel.friendly_name, uniqueName: channel.unique_name);
            if (!channels.Sid.Equals(""))
            {
                return RedirectToAction("Channels");
            }
            return View();
        }
        //
        [HttpGet]
        public ActionResult UpdateChannel(string sid)
        {
            try
            {
                TwilioClient.Init(GlobalVariables.accountSid, GlobalVariables.authToken);
                var channels = ChannelResource.Read(pathServiceSid: GlobalVariables.pathServiceSid);
                Channel ochannel = new Channel();
                List<Channel> list = new List<Channel>();
                foreach (var item in channels)
                {
                    if (item.Sid.Equals(sid))
                    {
                        Channel channel = new Channel
                        {
                            sid = String.IsNullOrEmpty(item.Sid) ? "" : item.Sid.ToString(),
                            account_sid = String.IsNullOrEmpty(item.AccountSid) ? "" : item.AccountSid.ToString(),
                            service_sid = String.IsNullOrEmpty(item.ServiceSid) ? "" : item.ServiceSid.ToString(),
                            friendly_name = String.IsNullOrEmpty(item.FriendlyName) ? "" : item.FriendlyName.ToString(),
                            unique_name = String.IsNullOrEmpty(item.UniqueName) ? "" : item.UniqueName.ToString()
                        };
                        ochannel = channel;
                        break;
                    }
                }
                return View(ochannel);
            }
            catch (Exception)
            {
                return Redirect(HttpContext.Request.UrlReferrer.AbsoluteUri);
            }
        }

        [HttpPost]
        public ActionResult UpdateChannel(Channel channel)
        {
            TwilioClient.Init(GlobalVariables.accountSid, GlobalVariables.authToken);
            
            var result = ChannelResource.Update(
                friendlyName: channel.friendly_name,
                uniqueName: channel.unique_name,
                pathServiceSid: GlobalVariables.pathServiceSid,
                pathSid: channel.sid

            );
            if (!result.Sid.Equals(""))
            {
                return RedirectToAction("Channels");
            }
            return View();
        }

        [HttpGet]
        public ActionResult DeleteChannel(string sid)
        {
            TwilioClient.Init(GlobalVariables.accountSid, GlobalVariables.authToken);
            ChannelResource.Delete(
                pathServiceSid: GlobalVariables.pathServiceSid,
                pathSid: sid
            );
            return RedirectToAction("Channels");
        }
    }
}