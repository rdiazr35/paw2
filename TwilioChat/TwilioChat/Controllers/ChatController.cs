﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Mvc;
using Twilio;
using Twilio.Rest.Chat.V2;
using Twilio.Rest.Chat.V2.Service;
using Twilio.Rest.Chat.V2.Service.Channel;
using Twilio.Rest.Chat.V2.Service.User;
using TwilioChat.Models;

namespace TwilioChat.Controllers
{
    public class ChatController : Controller
    {
        public static string identity = "";
        public ActionResult Index()
        {
            TwilioClient.Init(GlobalVariables.accountSid, GlobalVariables.authToken);
            var channels = ChannelResource.Read(GlobalVariables.pathServiceSid);
            Channel channel = new Channel();
            List<Channel> list = new List<Channel>();
            foreach (var item in channels)
            {
                channel = new Channel();
                channel.sid = String.IsNullOrEmpty(item.Sid) ? "" : item.Sid.ToString();
                channel.account_sid = String.IsNullOrEmpty(item.AccountSid) ? "" : item.AccountSid.ToString();
                channel.service_sid = String.IsNullOrEmpty(item.ServiceSid) ? "" : item.ServiceSid.ToString();
                channel.friendly_name = String.IsNullOrEmpty(item.FriendlyName) ? "" : item.FriendlyName.ToString();
                channel.unique_name = String.IsNullOrEmpty(item.UniqueName) ? "" : item.UniqueName.ToString();
                list.Add(channel);
            }
            string json = JsonConvert.SerializeObject(list);
            var listado = JsonConvert.DeserializeObject<List<Channel>>(json.ToString());
            return View(listado);
        }

        [HttpPost]
        public ActionResult ChatRoom(FormCollection collection)
        {
            string name = Convert.ToString(collection["nickname"]);
            string channel = Convert.ToString(collection["radioChannel"]);
            if (name.Equals(""))
            {
                GlobalVariables.pathChannelSid = channel;
                return Redirect(HttpContext.Request.UrlReferrer.AbsoluteUri);
            }
            if (channel.Equals(""))
            {
                return Redirect(HttpContext.Request.UrlReferrer.AbsoluteUri);
            }
            TwilioClient.Init(GlobalVariables.accountSid, GlobalVariables.authToken);
            var members = Twilio.Rest.Chat.V2.Service.Channel.MemberResource.Read(
                pathServiceSid: GlobalVariables.pathServiceSid,
                pathChannelSid: channel
            );
            //VERIFICAR SI EL NICKNAME YA PERTENECE A LOS USUARIOS DEL CANAL
            foreach (var record in members)
            {
                if (name.Equals(record.Identity) && channel.Equals(record.ChannelSid))
                {
                    GlobalVariables.GetUserName = name;
                    GlobalVariables.pathChannelSid = channel;
                    return RedirectToAction("ChatRoom");
                }
            }
            //CREACION DE MEMBER
            var member = Twilio.Rest.Chat.V2.Service.Channel.MemberResource.Create(
                identity: name,
                pathServiceSid: GlobalVariables.pathServiceSid,
                pathChannelSid: channel
            );
            if (member.Sid.Equals(""))
            {
                return Redirect(HttpContext.Request.UrlReferrer.AbsoluteUri);
            }
            GlobalVariables.GetUserName = name;
            GlobalVariables.pathChannelSid = channel;
            return RedirectToAction("ChatRoom");
        }

        [HttpGet]
        public ActionResult ChatRoom()
        {
            try
            {
                TwilioClient.Init(GlobalVariables.accountSid, GlobalVariables.authToken);
                var members = Twilio.Rest.Chat.V2.Service.Channel.MemberResource.Read(GlobalVariables.pathServiceSid, GlobalVariables.pathChannelSid);
                Member member = new Member();
                List<Member> listMember = new List<Member>();
                foreach (var item in members)
                {
                    member = new Member();
                    member.sid = String.IsNullOrEmpty(item.Sid) ? "" : item.Sid.ToString();
                    member.account_sid = String.IsNullOrEmpty(item.AccountSid) ? "" : item.AccountSid.ToString();
                    member.service_sid = String.IsNullOrEmpty(item.ServiceSid) ? "" : item.ServiceSid.ToString();
                    member.channel_sid = String.IsNullOrEmpty(item.ChannelSid) ? "" : item.ChannelSid.ToString();
                    member.identity = String.IsNullOrEmpty(item.Identity) ? "" : item.Identity.ToString();
                    listMember.Add(member);
                }
                //

                TwilioClient.Init(GlobalVariables.accountSid, GlobalVariables.authToken);
                var mess = Twilio.Rest.Chat.V2.Service.Channel.MessageResource.Read(GlobalVariables.pathServiceSid, GlobalVariables.pathChannelSid);
                Message message = new Message();
                List<Message> listMessage = new List<Message>();
                foreach (var item in mess)
                {
                    message = new Message();
                    message.sid = String.IsNullOrEmpty(item.Sid) ? "" : item.Sid.ToString();
                    message.account_sid = String.IsNullOrEmpty(item.AccountSid) ? "" : item.AccountSid.ToString();
                    message.service_sid = String.IsNullOrEmpty(item.ServiceSid) ? "" : item.ServiceSid.ToString();
                    message.channel_sid = String.IsNullOrEmpty(item.ChannelSid) ? "" : item.ChannelSid.ToString();
                    message.body = String.IsNullOrEmpty(item.Body) ? "" : item.Body.ToString();
                    message.from = String.IsNullOrEmpty(item.From) ? "" : item.From.ToString();
                    listMessage.Add(message);
                }
                MessageMember messageMember = new MessageMember();
                messageMember.ListMember = listMember;
                messageMember.ListMessage = listMessage;
                messageMember.Nickname = GlobalVariables.GetUserName;

                return View(messageMember);
            }
            catch (Exception)
            {
                return RedirectToAction("Index");
            }
            
        }
        //
        [HttpPost]
        public ActionResult SendMessage(FormCollection collection)
        {
            string mensaje = Convert.ToString(collection["mensaje"]);
            string from = GlobalVariables.GetUserName;
            TwilioClient.Init(GlobalVariables.accountSid, GlobalVariables.authToken);

            var message = MessageResource.Create(
                body: mensaje,
                from: from,
                pathServiceSid: GlobalVariables.pathServiceSid,
                pathChannelSid: GlobalVariables.pathChannelSid
            );
            return Redirect(HttpContext.Request.UrlReferrer.AbsoluteUri);
        }
    }
}
