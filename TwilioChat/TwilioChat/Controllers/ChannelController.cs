﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using Twilio;
using Twilio.Rest.Chat.V2;
using Twilio.Rest.Chat.V2.Service;
using Twilio.Rest.Chat.V2.Service.Channel;
using Twilio.Rest.Chat.V2.Service.User;
using TwilioChat.Models;


namespace TwilioChat.Controllers
{
    public class ChannelController : Controller
    {
        public ActionResult Index()
        {
            TwilioClient.Init(GlobalVariables.accountSid, GlobalVariables.authToken);
            var channels = ChannelResource.Read(GlobalVariables.pathServiceSid);
            Channel channel = new Channel();
            List<Channel> list = new List<Channel>();
            foreach (var item in channels)
            {
                channel.sid = String.IsNullOrEmpty(item.Sid) ? "" : item.Sid.ToString();
                channel.account_sid = String.IsNullOrEmpty(item.AccountSid) ? "" : item.AccountSid.ToString(); 
                channel.service_sid = String.IsNullOrEmpty(item.ServiceSid) ? "" : item.ServiceSid.ToString();
                channel.friendly_name = String.IsNullOrEmpty(item.FriendlyName) ? "" : item.FriendlyName.ToString();  
                channel.unique_name = String.IsNullOrEmpty(item.UniqueName) ? "" : item.UniqueName.ToString();
                list.Add(channel);
            }
            string json = JsonConvert.SerializeObject(list);
            var listado = JsonConvert.DeserializeObject<List<Channel>>(json.ToString());
            return View(listado);
        }
    }
}
