﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwilioChat.Models
{
    public class User
    {
        public string sid { get; set; }
        public string account_sid { get; set; }
        public string service_sid { get; set; }
        public string role_sid { get; set; }
        public string identity { get; set; }
        public object attributes { get; set; }
        public bool is_online { get; set; }
        public object is_notifiable { get; set; }
        public object friendly_name { get; set; }
        public DateTime date_created { get; set; }
        public DateTime date_updated { get; set; }
        public int joined_channels_count { get; set; }
        public string url { get; set; }
    }

}
