﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwilioChat.Models
{
    public class Channel
    {
        public string sid { get; set; }
        public string account_sid { get; set; }
        public string service_sid { get; set; }
        public string friendly_name { get; set; }
        public string unique_name { get; set; }
        public string type { get; set; }
        public DateTime date_created { get; set; }
        public DateTime date_updated { get; set; }
        public string created_by { get; set; }
        public int members_count { get; set; }
        public int messages_count { get; set; }
        public string url { get; set; }
    }
}
