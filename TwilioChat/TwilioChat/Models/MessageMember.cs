﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TwilioChat.Models
{
    public class MessageMember
    {
        public List<Member> ListMember { get; set; }
        public List<Message> ListMessage { get; set; }
        public string Nickname { get; set; }
    }
}