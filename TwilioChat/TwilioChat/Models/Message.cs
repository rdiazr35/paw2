﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwilioChat.Models
{
    public class Message
    {
        public string sid { get; set; }
        public string account_sid { get; set; }
        public string service_sid { get; set; }
        public string to { get; set; }
        public string channel_sid { get; set; }
        public DateTime date_created { get; set; }
        public DateTime date_updated { get; set; }
        public object last_updated_by { get; set; }
        public bool was_edited { get; set; }
        public string from { get; set; }
        public string body { get; set; }
        public int index { get; set; }
        public string type { get; set; }
        public string url { get; set; }
    }

}
