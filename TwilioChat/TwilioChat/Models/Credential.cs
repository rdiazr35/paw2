﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwilioChat.Models
{
    public class Credential
    {
        public string sid { get; set; }
        public string account_sid { get; set; }
        public string friendly_name { get; set; }
        public string type { get; set; }
        public string sandbox { get; set; }
        public DateTime date_created { get; set; }
        public DateTime date_updated { get; set; }
        public string url { get; set; }
    }
}
