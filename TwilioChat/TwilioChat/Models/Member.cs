﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwilioChat.Models
{
    public class Member
    {
        public string sid { get; set; }
        public string account_sid { get; set; }
        public string channel_sid { get; set; }
        public string service_sid { get; set; }
        public string identity { get; set; }
        public string role_sid { get; set; }
        public object last_consumed_message_index { get; set; }
        public object last_consumption_timestamp { get; set; }
        public DateTime date_created { get; set; }
        public DateTime date_updated { get; set; }
        public string url { get; set; }
    }
}
