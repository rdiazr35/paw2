﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwilioChat.Models
{
    public class RoleResource
    {

        public class Rootobject
        {
            public Meta meta { get; set; }
            public Role[] roles { get; set; }
        }

        public class Meta
        {
            public int page { get; set; }
            public int page_size { get; set; }
            public string first_page_url { get; set; }
            public string previous_page_url { get; set; }
            public string url { get; set; }
            public string next_page_url { get; set; }
            public string key { get; set; }
        }

        public class Role
        {
            public string sid { get; set; }
            public string account_sid { get; set; }
            public string service_sid { get; set; }
            public string friendly_name { get; set; }
            public string type { get; set; }
            public string[] permissions { get; set; }
            public DateTime date_created { get; set; }
            public DateTime date_updated { get; set; }
            public string url { get; set; }
        }

    }
}
