﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JSONParser
{
    class UserResource
    {

        public class Rootobject
        {
            public Meta meta { get; set; }
            public User[] users { get; set; }
        }

        public class Meta
        {
            public int page { get; set; }
            public int page_size { get; set; }
            public string first_page_url { get; set; }
            public string previous_page_url { get; set; }
            public string url { get; set; }
            public string next_page_url { get; set; }
            public string key { get; set; }
        }

        public class User
        {
            public string sid { get; set; }
            public string account_sid { get; set; }
            public string service_sid { get; set; }
            public string role_sid { get; set; }
            public string identity { get; set; }
            public object attributes { get; set; }
            public bool is_online { get; set; }
            public object is_notifiable { get; set; }
            public object friendly_name { get; set; }
            public DateTime date_created { get; set; }
            public DateTime date_updated { get; set; }
            public int joined_channels_count { get; set; }
            public Links links { get; set; }
            public string url { get; set; }
        }

        public class Links
        {
            public string user_channels { get; set; }
            public string user_bindings { get; set; }
        }

    }
}
