﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JSONParser
{
    class MessageResource
    {

        public class Rootobject
        {
            public Meta meta { get; set; }
            public Message[] messages { get; set; }
        }

        public class Meta
        {
            public int page { get; set; }
            public int page_size { get; set; }
            public string first_page_url { get; set; }
            public string previous_page_url { get; set; }
            public string url { get; set; }
            public string next_page_url { get; set; }
            public string key { get; set; }
        }

        public class Message
        {
            public string sid { get; set; }
            public string account_sid { get; set; }
            public string service_sid { get; set; }
            public string to { get; set; }
            public string channel_sid { get; set; }
            public DateTime date_created { get; set; }
            public DateTime date_updated { get; set; }
            public object last_updated_by { get; set; }
            public bool was_edited { get; set; }
            public string from { get; set; }
            public Attributes attributes { get; set; }
            public string body { get; set; }
            public int index { get; set; }
            public string type { get; set; }
            public Media media { get; set; }
            public string url { get; set; }
        }

        public class Attributes
        {
        }

        public class Media
        {
            public string sid { get; set; }
            public long size { get; set; }
            public string content_type { get; set; }
            public string filename { get; set; }
        }

    }
}
