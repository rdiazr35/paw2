﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JSONParser
{
    class UserChannelResource
    {

        public class Rootobject
        {
            public Meta meta { get; set; }
            public Channel[] channels { get; set; }
        }

        public class Meta
        {
            public int page { get; set; }
            public int page_size { get; set; }
            public string first_page_url { get; set; }
            public string previous_page_url { get; set; }
            public string url { get; set; }
            public string next_page_url { get; set; }
            public string key { get; set; }
        }

        public class Channel
        {
            public string account_sid { get; set; }
            public string service_sid { get; set; }
            public string channel_sid { get; set; }
            public string user_sid { get; set; }
            public string member_sid { get; set; }
            public string status { get; set; }
            public int last_consumed_message_index { get; set; }
            public int unread_messages_count { get; set; }
            public string notification_level { get; set; }
            public string url { get; set; }
            public Links links { get; set; }
        }

        public class Links
        {
            public string channel { get; set; }
            public string member { get; set; }
        }

    }
}
