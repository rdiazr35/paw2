﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JSONParser
{
    class MemberResource
    {

        public class Rootobject
        {
            public Meta meta { get; set; }
            public Member[] members { get; set; }
        }

        public class Meta
        {
            public int page { get; set; }
            public int page_size { get; set; }
            public string first_page_url { get; set; }
            public string previous_page_url { get; set; }
            public string url { get; set; }
            public string next_page_url { get; set; }
            public string key { get; set; }
        }

        public class Member
        {
            public string sid { get; set; }
            public string account_sid { get; set; }
            public string channel_sid { get; set; }
            public string service_sid { get; set; }
            public string identity { get; set; }
            public string role_sid { get; set; }
            public object last_consumed_message_index { get; set; }
            public object last_consumption_timestamp { get; set; }
            public DateTime date_created { get; set; }
            public DateTime date_updated { get; set; }
            public string url { get; set; }
        }

    }
}
