﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace JSONParser
{
    public partial class FrmDeserialiseJSON : Form
    {
        public FrmDeserialiseJSON()
        {
            InitializeComponent();
        }

        private void BtnDeserialise_Click(object sender, EventArgs e)
        {
            //this.mmOutput.Text = this.mmJSON.Text;
            //this.DebugOutput(this.mmJSON.Text);
            DeserialiseJSON(this.mmJSON.Text);
        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            this.mmOutput.Text = string.Empty;
        }

        private void BtnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        internal void DebugOutput(string json)
        {
            try
            {
                System.Diagnostics.Debug.Write(json + Environment.NewLine);
                mmOutput.Text = mmOutput.Text + json + Environment.NewLine;
                mmOutput.SelectionStart = mmOutput.Text.Length;
                mmOutput.ScrollToCaret();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Write(ex.Message.ToString() + Environment.NewLine);
            }
        }
        internal void DeserialiseJSON(string strJSON)
        {
            try
            {
                /*
                var jPerson = JsonConvert.DeserializeObject<dynamic>(strJSON);
                DebugOutput("" + jPerson.ToString());
                */
                var jPerson = JsonConvert.DeserializeObject<ServiceResource.Rootobject>(strJSON);
                this.DebugOutput("" + jPerson.services[0].sid.ToString());
            }
            catch (Exception ex)
            {
                DebugOutput("Tenemos un problema: " + ex.Message.ToString());
            }
        }
    }
}
