﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JSONParser
{
    class ChannelResource
    {

        public class Rootobject
        {
            public Channel[] channels { get; set; }
            public Meta meta { get; set; }
        }

        public class Meta
        {
            public int page { get; set; }
            public int page_size { get; set; }
            public string first_page_url { get; set; }
            public string previous_page_url { get; set; }
            public string url { get; set; }
            public string next_page_url { get; set; }
            public string key { get; set; }
        }

        public class Channel
        {
            public string sid { get; set; }
            public string account_sid { get; set; }
            public string service_sid { get; set; }
            public string friendly_name { get; set; }
            public string unique_name { get; set; }
            public Attributes attributes { get; set; }
            public string type { get; set; }
            public DateTime date_created { get; set; }
            public DateTime date_updated { get; set; }
            public string created_by { get; set; }
            public int members_count { get; set; }
            public int messages_count { get; set; }
            public string url { get; set; }
            public Links links { get; set; }
        }

        public class Attributes
        {
            public string foo { get; set; }
        }

        public class Links
        {
            public string members { get; set; }
            public string messages { get; set; }
            public string invites { get; set; }
            public string webhooks { get; set; }
            public object last_message { get; set; }
        }

    }
}
