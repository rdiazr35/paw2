﻿namespace JSONParser
{
    partial class FrmDeserialiseJSON
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mmJSON = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.mmOutput = new DevExpress.XtraEditors.MemoEdit();
            this.btnDeserialise = new DevExpress.XtraEditors.SimpleButton();
            this.btnClear = new DevExpress.XtraEditors.SimpleButton();
            this.btnSalir = new DevExpress.XtraEditors.SimpleButton();
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.mmJSON.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mmOutput.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // mmJSON
            // 
            this.mmJSON.Location = new System.Drawing.Point(12, 32);
            this.mmJSON.Name = "mmJSON";
            this.mmJSON.Properties.LookAndFeel.SkinName = "Black";
            this.mmJSON.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.mmJSON.Size = new System.Drawing.Size(776, 198);
            this.mmJSON.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(113, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "JSON (Serialised String)";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(12, 270);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(34, 13);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "Output";
            // 
            // mmOutput
            // 
            this.mmOutput.Location = new System.Drawing.Point(12, 290);
            this.mmOutput.Name = "mmOutput";
            this.mmOutput.Properties.LookAndFeel.SkinName = "Black";
            this.mmOutput.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.mmOutput.Size = new System.Drawing.Size(776, 198);
            this.mmOutput.TabIndex = 5;
            // 
            // btnDeserialise
            // 
            this.btnDeserialise.Location = new System.Drawing.Point(12, 237);
            this.btnDeserialise.LookAndFeel.SkinName = "Black";
            this.btnDeserialise.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnDeserialise.Name = "btnDeserialise";
            this.btnDeserialise.Size = new System.Drawing.Size(101, 23);
            this.btnDeserialise.TabIndex = 2;
            this.btnDeserialise.Text = "Deserialise";
            this.btnDeserialise.Click += new System.EventHandler(this.BtnDeserialise_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(119, 237);
            this.btnClear.LookAndFeel.SkinName = "Black";
            this.btnClear.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(101, 23);
            this.btnClear.TabIndex = 3;
            this.btnClear.Text = "Clear";
            this.btnClear.Click += new System.EventHandler(this.BtnClear_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(687, 494);
            this.btnSalir.LookAndFeel.SkinName = "Black";
            this.btnSalir.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(101, 23);
            this.btnSalir.TabIndex = 6;
            this.btnSalir.Text = "Salir";
            this.btnSalir.Click += new System.EventHandler(this.BtnSalir_Click);
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = this;
            this.bunifuDragControl1.Vertical = true;
            // 
            // FrmDeserialiseJSON
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 528);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnDeserialise);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.mmOutput);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.mmJSON);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmDeserialiseJSON";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmDeserialiseJSON";
            ((System.ComponentModel.ISupportInitialize)(this.mmJSON.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mmOutput.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.MemoEdit mmJSON;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.MemoEdit mmOutput;
        private DevExpress.XtraEditors.SimpleButton btnDeserialise;
        private DevExpress.XtraEditors.SimpleButton btnClear;
        private DevExpress.XtraEditors.SimpleButton btnSalir;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
    }
}

