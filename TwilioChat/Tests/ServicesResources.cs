﻿using System;

namespace Tests
{
    public class ServicesResources
    {
        public class Rootobject
        {
            public Meta meta { get; set; }
            public Service[] services { get; set; }
        }

        public class Meta
        {
            public string first_page_url { get; set; }
            public string key { get; set; }
            public string next_page_url { get; set; }
            public int page { get; set; }
            public int page_size { get; set; }
            public string previous_page_url { get; set; }
            public string url { get; set; }
        }

        public class Service
        {
            public string account_sid { get; set; }
            public int consumption_report_interval { get; set; }
            public DateTime date_created { get; set; }
            public DateTime date_updated { get; set; }
            public string default_channel_creator_role_sid { get; set; }
            public string default_channel_role_sid { get; set; }
            public string default_service_role_sid { get; set; }
            public string friendly_name { get; set; }
            public Limits limits { get; set; }
            public Links links { get; set; }
            public Notifications notifications { get; set; }
            public string post_webhook_url { get; set; }
            public string pre_webhook_url { get; set; }
            public int pre_webhook_retry_count { get; set; }
            public int post_webhook_retry_count { get; set; }
            public bool reachability_enabled { get; set; }
            public bool read_status_enabled { get; set; }
            public string sid { get; set; }
            public int typing_indicator_timeout { get; set; }
            public string url { get; set; }
            public string[] webhook_filters { get; set; }
            public string webhook_method { get; set; }
            public Media media { get; set; }
        }

        public class Limits
        {
            public int channel_members { get; set; }
            public int user_channels { get; set; }
        }

        public class Links
        {
            public string channels { get; set; }
            public string users { get; set; }
            public string roles { get; set; }
            public string bindings { get; set; }
        }

        public class Notifications
        {
        }

        public class Media
        {
            public int size_limit_mb { get; set; }
            public string compatibility_message { get; set; }
        }

    }
}
