﻿using System;
using System.Collections.Generic;
using Twilio;
using Twilio.Rest.Chat.V2;
using Twilio.Rest.Chat.V2.Service;
using Twilio.Rest.Chat.V2.Service.Channel;
using Twilio.Rest.Chat.V2.Service.User;

namespace Tests
{
    public class Program
    {
        const string accountSid = "SKb78d477b53854c0de76cb15d10aaa813";
        const string authToken = "xMNkbW4ISAR3qLp0ff9dHPaVEHLDJgGO";
        internal string sid = "";

        static void Main(string[] args)
        {
            //PrintUsers();
            //CreateChannel();
            //ShowChannels();
            //DeleteChannel();
            //ListAllServices();
            ListAllChannels();
            //UserChannel();
            //AddMember();
        }
        /*
        //
        public static void PrintUsers()
        {
            TwilioClient.Init(accountSid, authToken);

            var users = UserResource.Read(
                pathServiceSid: "IS0be6daaa1d6348fbb8d27004679de7d6"
            );

            foreach (var record in users)
            {
                Console.WriteLine(record.Sid);
            }
        }
        //
        public static bool ValidateAdmin()
        {
            TwilioClient.Init(accountSid, authToken);

            var users = UserResource.Read(
                pathServiceSid: "IS0be6daaa1d6348fbb8d27004679de7d6"
            );

            return true;
        }
        //
        public static void ShowChannels()
        {
            TwilioClient.Init(accountSid, authToken);

            var channels = ChannelResource.Read(
                pathServiceSid: "IS0be6daaa1d6348fbb8d27004679de7d6"
            );
            foreach (var record in channels)
            {
                Console.WriteLine(record.Sid);
            }
        }
        //
        public static void CreateChannel(string chat_name, string Sid)
        {
            TwilioClient.Init(accountSid, authToken);

            var channel = ChannelResource.Create(
                friendlyName: chat_name,
                pathServiceSid: Sid
            );

            Console.WriteLine(channel.Sid);
        }
        //
        public static void DeleteChannel()
        {
            TwilioClient.Init(accountSid, authToken);

            ChannelResource.Delete(
                pathServiceSid: "IS0be6daaa1d6348fbb8d27004679de7d6",
                pathSid: "CHf257224c1c234894817ee32decf44d94"//Channel id
            );
            ChannelResource.Delete(
                pathServiceSid: "IS0be6daaa1d6348fbb8d27004679de7d6",
                pathSid: "CH2ed3e5d3f21b4187b04b82b8cbe4f65e"//Channel id
            );
        }
        //
        public static void ReadMessages()
        {
            TwilioClient.Init(accountSid, authToken);

            var messages = MessageResource.Read(
                pathAccountSid: "CH796ee1d9a01542c3967e6e4692b9dbb6"
            );

            foreach (var record in messages)
            {
                Console.WriteLine(record.Sid);
            }
        }
        */
        //=========================================================================================================================
        //GET /Services
        public static void ListAllServices()
        {
            TwilioClient.Init(accountSid, authToken);
            var services = ServiceResource.Read();
            foreach (var record in services)
            {
                Console.WriteLine(record.Sid);
            }
        }
        //POST /Services
        public static void CreateService()
        {
            TwilioClient.Init(accountSid, authToken);
            var service = ServiceResource.Create(friendlyName: "FRIENDLY_NAME");
            Console.WriteLine(service.Sid);
        }
        //GET /Services/{Service SID}
        public static void RetrieveService()
        {
            TwilioClient.Init(accountSid, authToken);
            var service = ServiceResource.Fetch(pathSid: "ISXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
            Console.WriteLine(service.FriendlyName);
        }
        //POST /Services/{Service SID}
        public static void UpdateService()
        {
            var service = ServiceResource.Update(
                friendlyName: "NEW_FRIENDLY_NAME",
                pathSid: "ISXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            );
            Console.WriteLine(service.FriendlyName);
        }
        //DELETE /Services/{Service SID}
        public static void DeleteService()
        {
            TwilioClient.Init(accountSid, authToken);
            ServiceResource.Delete(pathSid: "ISXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        }
        //=========================================================================================================================
        //POST /Services/{Service SID}/Channels
        public static void ListAllChannels()
        {
            TwilioClient.Init(accountSid, authToken);
            var channels = ChannelResource.Read(
                pathServiceSid: "IS0be6daaa1d6348fbb8d27004679de7d6"
            );

            foreach (var record in channels)
            {
                Console.WriteLine(record.Sid);
            }
        }
        //GET /Services/{Service SID}/Channels/{Channel SID}
        public static void CreateChannel()
        {
            TwilioClient.Init(accountSid, authToken);
            var channel = ChannelResource.Create(
                friendlyName: "Mi canal",
                pathServiceSid: "IS0be6daaa1d6348fbb8d27004679de7d6"
            );
            Console.WriteLine(channel.Sid);
        }
        //POST /Services/{Service SID}/Channels/{Channel SID}
        public static void UpdateChannel()
        {
            TwilioClient.Init(accountSid, authToken);

            var channel = ChannelResource.Update(
                friendlyName: "NEW_FRIENDLY_NAME",
                pathServiceSid: "ISXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
                pathSid: "CHXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            );

            Console.WriteLine(channel.FriendlyName);
        }
        //DELETE /Services/{Service SID}/Channels/{Channel SID}
        public static void DeleteChannel()
        {
            TwilioClient.Init(accountSid, authToken);
            ChannelResource.Delete(
                pathServiceSid: "ISXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
                pathSid: "CHXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            );
        }
        //=========================================================================================================================
        //GET /Credentials
        public static void ListAllCredentials()
        {
            TwilioClient.Init(accountSid, authToken);
            var credentials = CredentialResource.Read();
            foreach (var record in credentials)
            {
                Console.WriteLine(record.Sid);
            }
        }
        //POST /Credentials
        public static void CreateCredential()
        {
            TwilioClient.Init(accountSid, authToken);

            var credential = CredentialResource.Create(
                apiKey: "apiKey",
                friendlyName: "Friendly Name",
                type: CredentialResource.PushServiceEnum.Gcm
            );
            Console.WriteLine(credential.Sid);
        }
        //GET /Credentials/{Credential SID}
        public static void GetCredential()
        {
            TwilioClient.Init(accountSid, authToken);
            var credential = CredentialResource.Fetch(
                pathSid: "CRXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            );

            Console.WriteLine(credential.FriendlyName);
        }
        //POST /Credentials/{Credential SID}
        public static void UpdateCredential()
        {
            TwilioClient.Init(accountSid, authToken);
            var credential = CredentialResource.Update(
                apiKey: "xxxxxx",
                friendlyName: "MyCredential",
                pathSid: "CRXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            );

            Console.WriteLine(credential.FriendlyName);
        }
        //DELETE /Credentials/{Credential SID}
        public static void DeleteCredential()
        {
            TwilioClient.Init(accountSid, authToken);
            CredentialResource.Delete(pathSid: "CRXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        }
        //=========================================================================================================================
        //GET /Services/{Service SID}/Channels/{Channel SID}/Members
        public static void ListAllMemberChannel()
        {
            TwilioClient.Init(accountSid, authToken);

            var members = MemberResource.Read(
                pathServiceSid: "ISXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
                pathChannelSid: "CHXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            );

            foreach (var record in members)
            {
                Console.WriteLine(record.Sid);
            }
        }
        //POST /Services/{Service SID}/Channels/{Channel SID}/Members
        public static void AddMember()
        {
            TwilioClient.Init(accountSid, authToken);

            var member = MemberResource.Create(
                identity: "CARLITOS",//Identity for USER
                pathServiceSid: "IS0be6daaa1d6348fbb8d27004679de7d6",//SERVICE ID
                pathChannelSid: "CH0f65405d08f64853bae79eff3fe61316"//CHANNEL ID
            );

            Console.WriteLine(member.Sid);
        }
        //GET /Services/{Service SID}/Channels/{Channel SID}/Members/{Member SID}
        public static void FetchMember()
        {
            TwilioClient.Init(accountSid, authToken);

            var member = MemberResource.Fetch(
                pathServiceSid: "ISXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
                pathChannelSid: "CHXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
                pathSid: "MBXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            );

            Console.WriteLine(member.Sid);
        }
        //DELETE /Services/{Service SID}/Channels/{Channel SID}/Members/{Member SID}
        public static void RemoveMemberChannel()
        {
            TwilioClient.Init(accountSid, authToken);
            MemberResource.Delete(
                pathServiceSid: "ISXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
                pathChannelSid: "CHXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
                pathSid: "IDENTITY"
            );
        }
        //=========================================================================================================================
        //GET /Services/{Service SID}/Channels/{Channel SID}/Messages
        public static void ListAllMessageChannel()
        {
            TwilioClient.Init(accountSid, authToken);

            var messages = MessageResource.Read(
                pathServiceSid: "ISXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
                pathChannelSid: "CHXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            );

            foreach (var record in messages)
            {
                Console.WriteLine(record.Sid);
            }
        }
        //POST /Services/{Service SID}/Channels/{Channel SID}/Messages
        public static void SendMessageChannel()
        {
            TwilioClient.Init(accountSid, authToken);

            var message = MessageResource.Create(
                body: "MESSAGE",
                pathServiceSid: "ISXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
                pathChannelSid: "CHXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            );
            Console.WriteLine(message.Sid);
        }
        //GET /Services/{Service SID}/Channels/{Channel SID}/Messages/{Message SID}
        public static void RetrieveMessageChannel()
        {
            TwilioClient.Init(accountSid, authToken);

            var message = MessageResource.Fetch(
                pathServiceSid: "ISXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
                pathChannelSid: "CHXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
                pathSid: "IMXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            );

            Console.WriteLine(message.To);
        }
        //POST /Services/{Service SID}/Channels/{Channel SID}/Messages/{Message SID}
        public static void UpdateMessage()
        {
            TwilioClient.Init(accountSid, authToken);

            var message = MessageResource.Update(
                body: "MESSAGE",
                pathServiceSid: "ISXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
                pathChannelSid: "CHXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
                pathSid: "IMXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            );

            Console.WriteLine(message.To);
        }
        //DELETE /Services/{Service SID}/Channels/{Channel SID}/Messages/{Message SID}
        public static void DeleteMessageChannel()
        {
            TwilioClient.Init(accountSid, authToken);

            MessageResource.Delete(
                pathServiceSid: "ISXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
                pathChannelSid: "CHXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
                pathSid: "IMXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            );
        }
        //=========================================================================================================================
        //GET /Services/{Service SID}/Roles
        public static void ListAllRoles()
        {
            TwilioClient.Init(accountSid, authToken);

            var roles = RoleResource.Read(
                pathServiceSid: "ISXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            );

            foreach (var record in roles)
            {
                Console.WriteLine(record.Sid);
            }
        }
        //POST /Services/{Service SID}/Roles
        public static void CreateRole()
        {
            TwilioClient.Init(accountSid, authToken);

            var permission = new List<string> {
                "createChannel"
            };

            var role = RoleResource.Create(
                friendlyName: "new_role",
                type: RoleResource.RoleTypeEnum.Deployment,
                permission: permission,
                pathServiceSid: "ISXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            );

            Console.WriteLine(role.Sid);
        }
        //GET /Services/{Service SID}/Roles/{Role SID}
        public static void RetrieveRole()
        {
            TwilioClient.Init(accountSid, authToken);

            var role = RoleResource.Fetch(
                pathServiceSid: "ISXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
                pathSid: "RLXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            );

            Console.WriteLine(role.FriendlyName);
        }
        //POST /Services/{Service SID}/Roles/{Role SID}
        public static void UpdateRole()
        {
            TwilioClient.Init(accountSid, authToken);

            var permission = new List<string> {
                "sendMediaMessage"
            };

            var role = RoleResource.Update(
                permission: permission,
                pathServiceSid: "ISXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
                pathSid: "RLXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            );

            Console.WriteLine(role.FriendlyName);
        }
        //DELETE /Services/{Service SID}/Roles/{Role SID}
        public static void DeleteRole()
        {
            TwilioClient.Init(accountSid, authToken);

            RoleResource.Delete(
                pathServiceSid: "ISXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
                pathSid: "RLXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            );
        }
        //=========================================================================================================================
        //GET /Services/{Service SID}/Users
        public static void ListAllUsers()
        {
            TwilioClient.Init(accountSid, authToken);

            var users = UserResource.Read(
                pathServiceSid: "ISXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            );

            foreach (var record in users)
            {
                Console.WriteLine(record.Sid);
            }
        }
        //POST /Services/{Service SID}/Users
        public static void CreateUser()
        {
            TwilioClient.Init(accountSid, authToken);

            var user = UserResource.Create(
                identity: "IDENTITY",
                pathServiceSid: "ISXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
                friendlyName: "Friendly Name",
                roleSid: "RLXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            );

            Console.WriteLine(user.Sid);
        }
        //GET /Services/{Service SID}/Users/{User SID}
        public static void RetrieveUser()
        {
            TwilioClient.Init(accountSid, authToken);

            var user = UserResource.Fetch(
                pathServiceSid: "ISXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
                pathSid: "USXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            );

            Console.WriteLine(user.FriendlyName);
        }
        //POST /Services/{Service SID}/Users/{User SID}
        public static void UpdateUser()
        {
            TwilioClient.Init(accountSid, authToken);

            var user = UserResource.Update(
                roleSid: "RLXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
                pathServiceSid: "ISXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
                pathSid: "USXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            );

            Console.WriteLine(user.FriendlyName);
        }
        //DELETE /Services/{Service SID}/Users/{User SID}
        public static void DeleteUser()
        {
            TwilioClient.Init(accountSid, authToken);

            UserResource.Delete(
                pathServiceSid: "ISXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
                pathSid: "USXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            );
        }
        //=========================================================================================================================
        //GET /Services/{Instance SID}/Users/{User SID}/Channels
        public static void ListAllUserChannel()
        {
            TwilioClient.Init(accountSid, authToken);

            var userChannels = UserChannelResource.Read(
                pathServiceSid: "ISXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
                pathUserSid: "USXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            );

            foreach (var record in userChannels)
            {
                Console.WriteLine(record.ServiceSid);
            }
        }
        //
        public static void UserChannel()
        {
            TwilioClient.Init(accountSid, authToken);

            var userChannels = UserChannelResource.Fetch(
                pathServiceSid: "IS0be6daaa1d6348fbb8d27004679de7d6",
                pathUserSid: "US9907ebcad8e94e399d8222c2afe97f1a",
                pathChannelSid: "CH0f65405d08f64853bae79eff3fe61316"
            );

            Console.WriteLine(userChannels.Status);
        }
        //

    }
}
