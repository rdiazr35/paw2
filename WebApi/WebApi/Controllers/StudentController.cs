﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using WebApiB.Modelo;

namespace WebApi.Controllers
{
    [Authorize]
    public class StudentController : ApiController
    {
        WebApiConnection webApiConnection = new WebApiConnection();

        [BasicAuthentication]
        public HttpResponseMessage Get()
        {
            string username = Thread.CurrentPrincipal.Identity.Name;
            switch (username.ToLower())
            {
                case "admin":
                    return Request.CreateResponse(HttpStatusCode.OK, webApiConnection.users.ToList());
                default:
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        /*
        // GET api/student
        /// <summary>
        /// Consulta de todos los estudiantes
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<student> Get()
        {
            string username = Thread.CurrentPrincipal.Identity.Name;
            var listado = this.webApiConnection.student.ToList();
            return listado;
        }
        */

        // GET api/student
        /// <summary>
        /// Consulta de todos los estudiantes
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public student Get(int studentID)
        {
            var libro = this.webApiConnection.student.FirstOrDefault(x=> x.studentID == studentID);
            return libro;
        }

        // POST api/student
        /// <summary>
        /// Insercion de datos
        /// </summary>
        /// <param name="student"></param>
        /// <returns></returns>
        [HttpPost]
        public bool Post(student student)
        {
            this.webApiConnection.student.Add(student);
            return this.webApiConnection.SaveChanges() > 0;
        }

        //PUT api/student
        /// <summary>
        /// Actualizacion de datos
        /// </summary>
        /// <param name="student"></param>
        /// <returns></returns>
        [HttpPut]
        public bool Put(student student)
        {
            var studentUpdate = this.webApiConnection.student.FirstOrDefault(x => x.studentID == student.studentID);
            studentUpdate.firstname = student.firstname;
            studentUpdate.lastname = student.lastname;
            studentUpdate.email = student.email;
            studentUpdate.address = student.address;
            return this.webApiConnection.SaveChanges() > 0;
        }

        //DELETE api/student
        /// <summary>
        /// Eliminacion de estudiante
        /// </summary>
        /// <param name="student"></param>
        /// <returns></returns>
        [HttpDelete]
        public bool Delete(int studentID)
        {
            var studentDelete = this.webApiConnection.student.FirstOrDefault(x => x.studentID == studentID);
            this.webApiConnection.student.Remove(studentDelete);
            return webApiConnection.SaveChanges() > 0;
        }

        
    }
}
