﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiB.Modelo;

namespace WebApi
{
    public class StudentSecurity
    {
        public static bool Login(string username, string password)
        {
            using(WebApiConnection webApiConnection = new WebApiConnection())
            {
                return webApiConnection.users.Any(user => user.username.Equals(username,
                    StringComparison.OrdinalIgnoreCase) && user.password == password);
            }
        }
        
    }
}