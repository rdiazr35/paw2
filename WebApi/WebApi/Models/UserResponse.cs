﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace WebApi.Models
{
    public class UserResponse
    {
        public HttpResponseMessage responseMsg { get; set; }
    }
}