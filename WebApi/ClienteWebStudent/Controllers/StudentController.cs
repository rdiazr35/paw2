﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using ClienteWebStudentB;
using System.Net.Http.Formatting;

namespace ClienteWebStudent.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult Index()
        {
            HttpClient studentClientHttp = new HttpClient();
            studentClientHttp.BaseAddress = new Uri("http://localhost:2135/");
            var request = studentClientHttp.GetAsync("api/Student").Result;
            if (request.IsSuccessStatusCode)//200
            {
                var resultString = request.Content.ReadAsStringAsync().Result;
                var listado = JsonConvert.DeserializeObject<List<Student>>(resultString);
                return View(listado);
            }
            return View(new List<Student>());
        }

        [HttpGet]
        public ActionResult NewStudent()
        {
            return View();
        }

        [HttpPost]
        public ActionResult NewStudent(Student student)
        {
            HttpClient studentClientHttp = new HttpClient();
            studentClientHttp.BaseAddress = new Uri("http://localhost:2135/");
            var request = studentClientHttp.PostAsync("api/Student", student, new JsonMediaTypeFormatter()).Result;
            if (request.IsSuccessStatusCode)//200
            {
                var resultString = request.Content.ReadAsStringAsync().Result;
                var resultado = JsonConvert.DeserializeObject<bool>(resultString);
                if (resultado)
                {
                    return RedirectToAction("Index");
                }
                return View(student);
            }
            return View(student);
        }

        [HttpGet]
        public ActionResult UpdateStudent(int studentID)
        {
            HttpClient studentClientHttp = new HttpClient();
            studentClientHttp.BaseAddress = new Uri("http://localhost:2135/");
            var request = studentClientHttp.GetAsync("api/Student?studentID=" + studentID).Result;
            if (request.IsSuccessStatusCode)//200
            {
                var resultString = request.Content.ReadAsStringAsync().Result;
                var info = JsonConvert.DeserializeObject<Student>(resultString);
                return View(info);
            }
            return View();
        }

        [HttpPost]
        public ActionResult UpdateStudent(Student student)
        {
            HttpClient studentClientHttp = new HttpClient();
            studentClientHttp.BaseAddress = new Uri("http://localhost:2135/");
            var request = studentClientHttp.PutAsync("api/Student", student, new JsonMediaTypeFormatter()).Result;
            if (request.IsSuccessStatusCode)//200
            {
                var resultString = request.Content.ReadAsStringAsync().Result;
                var resultado = JsonConvert.DeserializeObject<bool>(resultString);
                if (resultado)
                {
                    return RedirectToAction("Index");
                }
                return View(student);
            }
            return View(student);
        }

        [HttpGet]
        public ActionResult DeleteStudent(int studentID)
        {
            HttpClient studentClientHttp = new HttpClient();
            studentClientHttp.BaseAddress = new Uri("http://localhost:2135/");
            var request = studentClientHttp.DeleteAsync("api/Student?studentID=" + studentID).Result;
            if (request.IsSuccessStatusCode)//200
            {
                var resultString = request.Content.ReadAsStringAsync().Result;
                var resultado = JsonConvert.DeserializeObject<bool>(resultString);
                if (resultado)
                {
                    return RedirectToAction("Index");
                }
            }
            return View();
        }

        [HttpGet]
        public ActionResult DetailStudent(int studentID)
        {
            HttpClient studentClientHttp = new HttpClient();
            studentClientHttp.BaseAddress = new Uri("http://localhost:2135/");
            var request = studentClientHttp.GetAsync("api/Student?studentID=" + studentID).Result;
            if (request.IsSuccessStatusCode)//200
            {
                var resultString = request.Content.ReadAsStringAsync().Result;
                var info = JsonConvert.DeserializeObject<Student>(resultString);
                return View(info);
            }
            return View();
        }
    }
}