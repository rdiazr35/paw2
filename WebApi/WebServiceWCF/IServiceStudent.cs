﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WebApiB.Modelo;

namespace WebServiceWCF
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el 
    // nombre de interfaz "IServiceStudent" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IServiceStudent
    {
        [OperationContract]
        List<student> GetStudents();
    }
}
