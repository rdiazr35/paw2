DROP TABLE IF EXISTS student;

CREATE TABLE student (
    studentID int NOT NULL IDENTITY(1,1),
    firstname varchar(255),
    lastname varchar(255),
	email varchar(255),
    address varchar(255),     
	PRIMARY KEY (studentID)
);

INSERT INTO student(firstname, lastname, email, address) VALUES
 ('Randy', 'D�az', 'randydiazruiz@gmail.com', 'Venecia, San Carlos');
INSERT INTO student(firstname, lastname, email, address) VALUES
 ('Carlos', 'Torres', 'ctorres@gmail.com', 'Aguas Zarcas, San Carlos');

SELECT * FROM student;