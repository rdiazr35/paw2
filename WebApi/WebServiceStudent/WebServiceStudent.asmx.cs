﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using WebApiB.Modelo;

namespace WebServiceStudent
{
    /// <summary>
    /// Descripción breve de WebServiceStudent
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebServiceStudent : System.Web.Services.WebService
    {
        WebApiConnection webApiConnection = new WebApiConnection();
        [WebMethod]
        public List<student> ObtenerLibros()
        {
            var listado = webApiConnection.student.ToList();
            return listado;
        }
    }
}
