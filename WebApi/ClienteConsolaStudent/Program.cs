﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ClienteConsolaStudent
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //INVOCAR SERVICIO ASMX
                WebServiceStudent.WebServiceStudentSoapClient studentClient = new WebServiceStudent.WebServiceStudentSoapClient();
                var libros = studentClient.ObtenerLibros();
                foreach (var item in libros)
                {
                    Console.WriteLine(item.firstname);
                }
                Console.WriteLine("----------------------------------------");
                //INVOCAR SERVICIO WCF
                ServiceStudentWCF.ServiceStudentClient studentClientWCF = new ServiceStudentWCF.ServiceStudentClient();
                libros = studentClient.ObtenerLibros();
                foreach (var item in libros)
                {
                    Console.WriteLine(item.firstname);
                }
                Console.WriteLine("----------------------------------------");
                //INVOCAR SERVICIO REST
                HttpClient studentClientHttp = new HttpClient();
                studentClientHttp.BaseAddress = new Uri("http://localhost:2135/");
                var request = studentClientHttp.GetAsync("api/Student").Result;
                if (request.IsSuccessStatusCode)//200
                {
                    var resultString = request.Content.ReadAsStringAsync().Result;
                    var listado = JsonConvert.DeserializeObject<List<Student>>(resultString);
                    foreach (var item in listado)
                    {
                        Console.WriteLine(item.firstname);
                    }
                }
                //studentClientHttp.PostAsync();
                //studentClientHttp.PutAsync();
                //studentClientHttp.DeleteAsync();
                Console.Read();
            }
            catch (Exception ex)
            {
                
            }
           

        }
    }
}
